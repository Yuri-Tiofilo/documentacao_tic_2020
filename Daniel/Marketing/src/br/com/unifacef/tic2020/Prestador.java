package br.com.unifacef.tic2020;

import java.util.ArrayList;

public final class Prestador extends Usuario{
    public String telefone;
    public float pontuacao;
    public String CPF;
    public String CNPJ;
    public ArrayList<Portfolio> portfolio;
    public ArrayList<Endereco> endereco;

    public Prestador() {
        super();
        this.portfolio = new ArrayList();
        this.endereco = new ArrayList();
    }

    public Prestador(String telefone, float pontuacao, String CPF, String CNPJ, String nome, String email, String senha, ArrayList<Portfolio> portfolio, ArrayList<Endereco> endereco) {
        super(nome, email, senha);
        this.setTelefone(telefone);
        this.setPontuacao(pontuacao);
        this.setCPF(CPF);
        this.setCNPJ(CNPJ);
        this.setPortfolio(portfolio);
        this.setEndereco(endereco);
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public float getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(float pontuacao) {
        this.pontuacao = pontuacao;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public ArrayList<Portfolio> getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(ArrayList<Portfolio> portfolio) {
        this.portfolio = portfolio;
    }

    public ArrayList<Endereco> getEndereco() {
        return endereco;
    }

    public void setEndereco(ArrayList<Endereco> endereco) {
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "Prestador{" + "telefone=" + getTelefone() 
                + ", pontuacao=" + getPontuacao() 
                + ", CPF=" + getCPF() 
                + ", CNPJ=" + getCNPJ() 
                + ", portfolio=" + getPortfolio() 
                + ", endereco=" + getEndereco() 
                + super.toString() + '}';
    }
    
  
    public void createPrestador(Cliente cliente){
        
    }
    public boolean deletePrestador(Prestador prestador){
        return true;
    }
    public int getPrestador(Prestador prestador){
        return 0;
    }
    public boolean updatePrestador(Prestador oldPrestador, Prestador newPrestador){
        return true;
    }
    public void agendar(){
        
    }
    public void addEndereco(){
        
    }
    public void addPortfolio(){
    
    }
}
