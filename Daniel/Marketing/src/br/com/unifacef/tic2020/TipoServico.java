package br.com.unifacef.tic2020;

public final class TipoServico {
    public float valor;
    public String descricao;
    public Number duracao;

    public TipoServico() {
    
    }

    public TipoServico(float valor, String descricao, Number duracao) {
        this.setValor(valor);
        this.setDescricao(descricao);
        this.setDuracao(duracao);
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Number getDuracao() {
        return duracao;
    }

    public void setDuracao(Number duracao) {
        this.duracao = duracao;
    }

    @Override
    public String toString() {
        return "TipoServico{" + "valor=" + this.getValor() 
                + ", descricao=" + this.getDescricao() 
                + ", duracao=" + this.getDuracao() + '}';
    }
    
    public void createTipoServico(TipoServico tipoServico){
    
    }
    public boolean deleteTipoServico(TipoServico tipoServico){
        return true;
    }
    public int getTipoServico(TipoServico tipoServico){
        return 0;
    }
    public boolean updateTipoServico(TipoServico oldTipoServico, TipoServico newTipoServico){
        return true;
    }
    
}
