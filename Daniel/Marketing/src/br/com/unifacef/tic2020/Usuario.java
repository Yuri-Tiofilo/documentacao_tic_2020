package br.com.unifacef.tic2020;

public class Usuario {
    protected String nome;
    protected String email;
    protected String senha;

    public Usuario() {
    
    }

    public Usuario(String nome, String email, String senha) {
        this.setNome(nome);
        this.setEmail(email);
        this.setSenha(senha);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public String toString() {
        return "Usuario{" + "nome=" + this.getNome() + ", email=" + this.getEmail() + ", senha=" + this.getSenha() + '}';
    }
    
    public void createUsuario(Usuario usuario){
        
    }
    
    public boolean deleteUsuario(Usuario usuario){
        return true;
    }
    
    public int getUsuario(Usuario usuario){
        return 0;
    }
    
    public boolean updateUsuario(Usuario oldUsuario, Usuario newUsuario){
        return true;
    }
}
