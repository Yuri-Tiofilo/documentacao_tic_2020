package br.com.unifacef.tic2020;

public final class Endereco {
    private String rua;
    private String bairro;
    private int numero;
    private String cidade;
    private String cep;
    private String uf;
    private String complemento;

    public Endereco() {
    
    }

    public Endereco(String rua, String bairro, int numero, String cidade, String cep, String uf, String complemento) {
        this.setRua(rua);
        this.setBairro(bairro);
        this.setNumero(numero);
        this.setCidade(cidade);
        this.setCep(cep);
        this.setUf(uf);
        this.setComplemento(complemento);
    }

    @Override
    public String toString() {
        return "Endereco{" + "rua=" + this.getRua() 
                + ", bairro=" + this.getBairro()
                + ", numero=" + this.getNumero() 
                + ", cidade=" + this.getCidade()
                + ", cep=" + this.getCep()
                + ", uf=" + this.getUf()
                + ", complemento=" + this.getComplemento() + '}';
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
    
    public void createEndereco(Endereco endereci){
        
    }
    
    public boolean deleteEndereco(Endereco endereco){
        return true;
    }
    
    public int getEndereco(Endereco endereco){
        return 0;
    }
    
    public boolean updateEndereco(Endereco oldEndereco, Endereco newEndereco){
        return true;
    }
    
}
