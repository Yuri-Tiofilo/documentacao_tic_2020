package br.com.unifacef.tic2020;

import java.util.ArrayList;

public final class Agenda {
    public int ano;
    public String descricao;
    public ArrayList<Prestador> prestador;

    public Agenda() {
        this.prestador = new ArrayList();
    }

    public Agenda(int ano, String descricao) {
        this.setAno(ano);
        this.setDescricao(descricao);
        
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ArrayList<Prestador> getPrestador() {
        return prestador;
    }

    public void setPrestador(ArrayList<Prestador> prestador) {
        this.prestador = prestador;
    }

    @Override
    public String toString() {
        return "Agenda{" + "ano=" + this.getAno() 
                + ", descricao=" + this.getDescricao() 
                + "prestador=" + this.getPrestador() +'}';
    }
    
    public void createAgenda(Agenda agenda){
        
    }
    public boolean deleteAgenda(Agenda agenda){
        return true;
    }
    public int getAgenda(Agenda agenda){
        return 0;
    }
    public boolean updateAgenda(Agenda oldAgenda, Agenda newAgenda){
        return true;
    }
    
}
