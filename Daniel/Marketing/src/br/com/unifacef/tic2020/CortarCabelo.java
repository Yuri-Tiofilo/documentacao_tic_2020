package br.com.unifacef.tic2020;

public class CortarCabelo extends TipoServico{
    private String tipoCorte;

    public CortarCabelo() {
        super();
    }

    public CortarCabelo(String tipoCorte, float valor, String descricao, Number duracao) {
        super(valor, descricao, duracao);
        this.tipoCorte = tipoCorte;
    }

    public String getTipoCorte() {
        return tipoCorte;
    }

    public void setTipoCorte(String tipoCorte) {
        this.tipoCorte = tipoCorte;
    }

    @Override
    public String toString() {
        return "CortarCabelo{" + "tipoCorte=" + tipoCorte + "| " + super.toString() + '}';
    }
    
    
    
}
