package br.com.unifacef.tic2020;

public final class Cliente extends Usuario{
    private String cpf;

    public Cliente() {
        super();
    }

    public Cliente(String cpf, String nome, String email, String senha) {
        super(nome, email, senha);
        this.setCpf(cpf);
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return "Cliente{" + "cpf=" + this.getCpf()+ "| " + super.toString() + '}';
    }
    
    public void createCliente(Cliente cliente){
        
    }
    
    public boolean deleteCliente(Cliente cliente){
        return true;
    }
    public int getCliente(Cliente cliente){
        return 0;
    }
    public boolean updateCliente(Cliente oldCliente, Cliente newCliente){
        return true;
    }
    public void agendar(){
        
    }
}
