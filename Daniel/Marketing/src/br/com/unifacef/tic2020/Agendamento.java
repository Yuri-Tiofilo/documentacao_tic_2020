package br.com.unifacef.tic2020;

import java.util.ArrayList;
import java.util.Date;

public final class Agendamento {
    public Date data;
    public Number horario;
    public String observacao;
    public ArrayList<TipoServico> tipoServico;
    public ArrayList<Agenda> agenda;
    public ArrayList<Cliente> cliente;

    public Agendamento() {
        this.tipoServico = new ArrayList();
        this.agenda = new ArrayList();
        this.cliente = new ArrayList();
    }

    public Agendamento(Date data, Number horario, String observacao, ArrayList<TipoServico> tipoServico, ArrayList<Agenda> agenda, ArrayList<Cliente> cliente) {
        this.setData(data);
        this.setHorario(horario);
        this.setObservacao(observacao);
        this.setTipoServico(tipoServico);
        this.setAgenda(agenda);
        this.setCliente(cliente);
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Number getHorario() {
        return horario;
    }

    public void setHorario(Number horario) {
        this.horario = horario;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public ArrayList<TipoServico> getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(ArrayList<TipoServico> tipoServico) {
        this.tipoServico = tipoServico;
    }

    public ArrayList<Agenda> getAgenda() {
        return agenda;
    }

    public void setAgenda(ArrayList<Agenda> agenda) {
        this.agenda = agenda;
    }

    public ArrayList<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(ArrayList<Cliente> cliente) {
        this.cliente = cliente;
    }
    

    @Override
    public String toString() {
        return "Agendamento{" + "data=" + this.getData() 
                + ", horario=" + this.getHorario()
                + ", observacao=" + this.getObservacao()
                + ", tipoServico=" + this.getTipoServico() 
                + ", agenda=" + this.getAgenda() 
                + ", cliente=" + this.getCliente() +'}';
    }
    
    public void createAgendamento(Agendamento agendamento){
        
    }
    public boolean deleteAgendamento(Agendamento agendamento){
        return true;
    }
    public int getAgendamento(Agendamento agendamento){
        return 0;
    }
    public boolean updateAgendamento(Agenda oldAgendamento, Agenda newAgendamento){
        return true;
    }
    public void addServico(){
        
    }
}
    
