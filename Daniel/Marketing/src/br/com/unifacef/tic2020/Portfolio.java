package br.com.unifacef.tic2020;

public final class Portfolio {
    public String nome;
    public String caminho;

    public Portfolio() {
    }

    public Portfolio(String nome, String caminho) {
        this.setNome(nome);
        this.setCaminho(caminho);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    @Override
    public String toString() {
        return "Portfolio{" + "nome=" + this.getNome() 
                + ", caminho=" + this.getCaminho() + '}';
    }
    
    public void createPortfolio(Portfolio portfolio){
        
    }
    public boolean deletePortfolio(Portfolio portfolio){
        return true;
    }
    public int getPortfolio(Portfolio portfolio){
        return 0;
    }
    public boolean updatePortfolio(Portfolio oldPortfolio, Portfolio newPortfolio){
        return true;
    }
    
    
}
