package br.com.unifacef.tic2020;

public class FazerUnha extends TipoServico{
    private String tipoUnha;

    public FazerUnha() {
        super();
    }

    public FazerUnha(String tipoUnha, float valor, String descricao, Number duracao) {
        super(valor, descricao, duracao);
        this.tipoUnha = tipoUnha;
    }

    public String getTipoUnha() {
        return tipoUnha;
    }

    public void setTipoUnha(String tipoUnha) {
        this.tipoUnha = tipoUnha;
    }

    @Override
    public String toString() {
        return "FazerUnha{" + "tipoUnha=" + tipoUnha + "| " + super.toString() +'}';
    }
    
    
}
