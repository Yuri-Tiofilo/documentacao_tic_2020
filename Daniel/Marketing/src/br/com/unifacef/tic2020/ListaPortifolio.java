package br.com.unifacef.tic2020;

public class ListaPortifolio {
    private String nome;
    private String caminho;

    public ListaPortifolio() {
    
    }

    public ListaPortifolio(String nome, String caminho) {
        this.nome = nome;
        this.caminho = caminho;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    @Override
    public String toString() {
        return "ListaPortifolio{" + "nome=" + nome + ", caminho=" + caminho + '}';
    }
    
    public void createPortifolio(ListaPortifolio listaPortifolio){
    
    }
    
    public boolean deleteUsuario(ListaPortifolio listaPortifolio){
        return true;
    }
    
    public int getUsuario(ListaPortifolio listaPortifolio){
        return 0;
    }
    
    public boolean updateUsuario(ListaPortifolio oldListaPortifolio, ListaPortifolio newListaPortifolio){
        return true;
    }
    
}
