CREATE TABLE tb_prestadores (
  id_prestador              INTEGER,
  id_endereco               INTEGER CONSTRAINT uq_tb_prestadores_id_endereco UNIQUE,
  nome                      VARCHAR(60) CONSTRAINT nn_tb_prestadores_nome NOT NULL,
  email                     VARCHAR(60) CONSTRAINT nn_tb_prestadores_email NOT NULL,
  senha                     VARCHAR CONSTRAINT nn_tb_prestadores_senha NOT NULL,
  descricao                 TEXT,
  horario_funcionamento     NUMERIC,
  permissao                 VARCHAR(10),
  pontuacao                 FLOAT,
  cpf                       VARCHAR(12),
  cnpj                      VARCHAR(15),
  CONSTRAINT pk_tb_prestador_id_prestador PRIMARY KEY (id_prestador),
  CONSTRAINT fk_tb_prestador_id_endereco FOREIGN KEY (id_endereco)
      REFERENCES tb_endereco (id_endereco)
)

CREATE TABLE tb_enderecos (
    id_endereco     INTEGER,
    rua             VARCHAR,
    numero          NUMERIC,
    bairro          VARCHAR,
    cep             VARCHAR(9),
    uf              VARCHAR(2),
    cidade          VARCHAR,
    CONSTRAINT pk_tb_enderecos_id_endereco PRIMARY KEY (id_endereco)
)

CREATE TABLE tb_arquivos (
  id_arquivo        INTEGER,
  id_prestador      INTEGER,
  caminho           VARCHAR,
  nome              VARCHAR,
  extencao          VARCHAR(8),
  CONSTRAINT pk_tb_arquivos_id_arquivo PRIMARY KEY (id_arquivo),
  CONSTRAINT fk_tb_arquivos_id_prestador FOREIGN KEY (id_prestador)
      REFERENCES tb_prestadores(id_prestador)
)

CREATE TABLE tb_servicos (
  id_servico        INTEGER,
  nome_servico      VARCHAR,
  descricao         TEXT,
  duracao           NUMERIC,
  categoria         VARCHAR,
  valor             FLOAT,
  CONSTRAINT pk_tb_servico_id_servico PRIMARY KEY (id_servico)
)

CREATE TABLE tb_prestadores_servicos (
  id_prestador      INTEGER,
  id_servico        INTEGER,
  data_tb_pivo      DATE,
  CONSTRAINT pk_tb_pres_serv_id_pres_serv PRIMARY KEY (id_prestador, id_servico),
  CONSTRAINT fk_tb_pres_serv_id_pres FOREIGN KEY (id_prestador)
      REFERENCES tb_prestadores(id_prestador),
  CONSTRAINT fk_tb_pres_serv_id_serv FOREIGN KEY (id_servicos)
      REFERENCES tb_servico(id_servicos)
)

CREATE TABLE tb_usuarios (
  id_usuario        INTEGER,
  nome              VARCHAR(60) CONSTRAINT nn_tb_prestadores_nome NOT NULL,
  email             VARCHAR(60) CONSTRAINT nn_tb_prestadores_email NOT NULL,
  senha             VARCHAR CONSTRAINT nn_tb_prestadores_senha NOT NULL,
  CONSTRAINT pk_tb_usuarios_id_usuario PRIMARY KEY (id_usuario)
)

CREATE TABLE tb_agendamentos (
  id_agendamento      INTEGER,
  id_usuario          INTEGER,
  id_prestador        INTEGER,
  id_servico          INTEGER,
  data_marcada        DATE,
  hora_marcada        NUMERIC,
  descricao           TEXT,  
  cancelamento        DATE,
  CONSTRAINT pk_tb_agendamentos_id_agendamento PRIMARY KEY (id_agendamento),
  CONSTRAINT fk_tb_agendamentos_id_usuario FOREIGN KEY (id_usuario)
      REFERENCES tb_usuarios(id_usuario),
  CONSTRAINT fk_tb_agendamentos_id_prestador FOREIGN KEY (id_prestador)
      REFERENCES tb_prestadores_servicos(id_prestador), 
  CONSTRAINT fk_tb_agendamentos_id_servico FOREIGN KEY (id_servico)
      REFERENCES tb_prestadores_servicos(id_servico),       
)